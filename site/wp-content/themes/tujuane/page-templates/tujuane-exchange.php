<?php
/**
 * Template Name: Tujuane Exchange
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 
?>
<?php get_sidebar( 'advert' ); ?>
<div class="row">
    <div id="main-content-inner" class="large-12 columns">
		<div class="tujuane-exchange">
        <?php
            // Start the Loop.
            while ( have_posts() ) : the_post();
                the_post_thumbnail('full');
        ?>
                
        <?php
                the_content();
				// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
            endwhile;
        ?>
		</div>
    </div>
    <!-- #primary -->
    <?php get_sidebar( 'content' ); ?>
</div>
<!-- #main-content -->

<?php get_footer();
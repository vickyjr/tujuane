<?php /** * The template for displaying Hook up page * * @package WordPress * @subpackage Twenty_Fourteen * @since Twenty Fourteen 1.0 */ get_header(); ?>
<?php get_sidebar( 'advert' ); ?>
<div class="row">
    <div id="main-content-inner" class="large-12 columns">
        <img width="3268" height="933" src="<?php echo get_template_directory_uri(); ?>/img/dating-tips-photo.jpg" class="attachment-full wp-post-image" alt="dating-tips-photo">
        <div class="large-11 columns centered posts">
            <div class="row collapse">
                <div class="column">
                    <h1 class="entry-title-large">Want to be on the show</h1>
                    <h2 class="small">Fill In The Details Below For A Free Hookup</h2>
                </div>
            </div>
            <form class="custom hook-up">
                <div class="row collapse">
                    <div class="small-3  columns">
                        <h4>Display Name:</h4>
                    </div>
                    <div class="small-8  columns">
                        <div class="row">
                            <input type="text" placeholder="Choose a display name">
                        </div>
                        <div class="row">
                            <p>(This will appear on your profile; no spaces)</p>
                        </div>
                    </div>
                </div>
                <div class="row collapse">
                    <div class="small-3 columns">
                        <h4>Password:</h4>
                    </div>
                    <div class="small-8  columns">
                        <div class="row">
                            <input type="password" placeholder="Choose a password">
                        </div>
                        <div class="row">
                            <p>(6-16 characters MUST include numbers; no spaces)</p>
                        </div>
                    </div>
                </div>
                <div class="row collapse">
                    <div class="small-3 columns ">
                        <h4>Email:</h4>
                    </div>
                    <div class="small-8 columns ">
                        <input type="email" placeholder="Enter your email address">
                    </div>
                </div>
                <div class="row collapse">
                    <div class="small-3 columns ">
                        <h4>I am a:</h4>
                    </div>
                    <div class="small-8 columns ">
                        <select id="gender" class="custom dropdown">
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                        </select>
                    </div>
                </div>
                <div class="row collapse">
                    <div class="small-3 columns ">
                        <h4>Seeking:</h4>
                    </div>
                    <div class="small-8 columns ">
                        <select id="seeking" class="custom dropdown">
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                        </select>
                    </div>
                </div>
                <div class="row collapse">
                    <div class="small-3 columns ">
                        <h4>Birthday:</h4>
                    </div>
                    <div class="small-8 columns ">
                        <div class="small-4 columns">
                            <select id="month" class="custom dropdown">
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="2">March</option>
                                <option value="2">April</option>
                                <option value="2">May</option>
                                <option value="2">June</option>
                                <option value="2">July</option>
                                <option value="2">August</option>
                                <option value="2">September</option>
                                <option value="2">October</option>
                                <option value="2">November</option>
                                <option value="2">December</option>
                            </select>
                        </div>
                        <div class="small-4 columns">
                            <select id="gender" class="custom dropdown">
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                        <div class="small-4 columns">
                            <select id="gender" class="custom dropdown">
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row collapse right">
                    <input type="submit" value="Submit" class="button" />
                </div>
            </form>
        </div>
    </div>
    <!-- #primary -->
    <?php get_sidebar( 'content' ); ?>
</div>
<!-- #main-content -->

<?php get_footer();
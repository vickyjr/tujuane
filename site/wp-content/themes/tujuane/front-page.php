<?php
/**
 * The home page template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */ 
?>
    <!DOCTYPE html>
    <!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
    <!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
    <!--[if !(IE 7) | !(IE 8) ]><!--><html <?php language_attributes(); ?>><!--<![endif]-->

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width">
        <title>
            <?php wp_title( '|', true, 'right' ); ?>
        </title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/foundation.css" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr.js"></script>
        <!--[if lt IE 9]><script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script><![endif]-->
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="home-logo">
            <div class="large-3 columns width-25">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="tujuane logo">
                </a>
            </div>
        </div>
        <div id="main-img">
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/tujuane.jpg" alt="Main photo">
                <div id="home-nav">
                    <div class="row">
                        <div class="large-12 columns">
                            <?php wp_nav_menu( array( 'menu'=>'Main menu', 'container_class' => '', 'menu_class' => 'pagination', 'container' => false ) ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="main-content" class="row">
            <div class="large-4 columns home-widgets">
                <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
                <?php dynamic_sidebar( 'sidebar-4' ); ?>
                <?php endif; ?>
            </div>
            <div class="large-4 columns home-widgets">
                <?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
                <?php dynamic_sidebar( 'sidebar-5' ); ?>
                <?php endif; ?>
            </div>
            <div class="large-4 columns home-widgets">
                <?php if ( is_active_sidebar( 'sidebar-6' ) ) : ?>
                <?php dynamic_sidebar( 'sidebar-6' ); ?>
                <?php endif; ?>
            </div>
        </div>

        <?php get_footer();
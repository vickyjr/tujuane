<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div id="footer" class="row">
  <div class="large-5 columns small-text"> Copyright © 2014 Tujuane. The Online Experience </div>
  <div class="large-3 columns small-text"> E: love@tujuane.tv </div>
  <div class="large-4 columns small-text">
   <?php wp_nav_menu( array( 
   'menu' => 'Footer menu', 
		  							'container_class' => 'large-12 columns', 
									'menu_class' => 'pagination', 
									'container' => false ) ); ?>
   
  </div>
</div>
	<?php wp_footer(); ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/foundation.min.js"></script> 
<script>
      //$(document).foundation();
    </script>
</body>
</html>
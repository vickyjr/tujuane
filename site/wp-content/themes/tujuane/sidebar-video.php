<?php
/**
 * The Content Sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

if ( ! is_active_sidebar( 'sidebar-8' ) ) {
	return;
}
?>

<div id="video" class="row">
  <div class="large-10 large-offset-1 columns">
    <?php dynamic_sidebar( 'sidebar-8' ); ?>
  </div>
</div>

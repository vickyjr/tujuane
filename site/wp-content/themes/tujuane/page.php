<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 
?>
<?php get_sidebar( 'advert' ); ?>
<div class="row">
    <div id="main-content-inner" class="large-12 columns">

        <?php
            // Start the Loop.
            while ( have_posts() ) : the_post();
                the_post_thumbnail('full');
        ?>
                
        <?php
                the_content();
            endwhile;
        ?>

    </div>
    <!-- #primary -->
    <?php get_sidebar( 'content' ); ?>
</div>
<!-- #main-content -->

<?php get_footer();
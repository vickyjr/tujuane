<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tujuane');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3N~~;U=87a@r(M8GQ^TAs*J{YqU%wV2734+el@4g%SLGuL.E(v|&Kr(^y+ `m#PD');
define('SECURE_AUTH_KEY',  '%|i|s(s|w%IqvRQ$&pQ-8/_>&<L#u)kSY+a.}!~=@3ViA3nE{f~p}$}+m>~%j;f?');
define('LOGGED_IN_KEY',    ' L+B*]n[-Hu5KUSb:`aX?nf_S3uq|/QHR_NG5h)cQh8hs.ntJ  j=D6{?bKU1odI');
define('NONCE_KEY',        '2-kSO55fV|F0`Evx?.:D1]P/BvUl-[o!bb+t9h~A*!XKC;Ny[rmjq2k|p!vR|={O');
define('AUTH_SALT',        '>&gSNF< r=7vNHI5gWKlgyu]D~]^u6n|y4d}%hPZ:1a1V<^B_T_$@wqU3.|OeD?@');
define('SECURE_AUTH_SALT', 'h~|i:o;5J]H^LP5v3v0!&6-|E nu,r2Best4x49h=FB>M;D(0e,pWOk)(xmQ8#G!');
define('LOGGED_IN_SALT',   '9 +d=be04q9X=TZND;kK_2cw0r_LH#GFU5)M{)bPnDuzXA*%jbA>_5^Z`bo=Os}/');
define('NONCE_SALT',       'FoX]Tij!G09oX~b)_5Qvq/%:}d8oN-(*| V|#ogb YvyJ(,U--`@WXiS^cvqNH3s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tj_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
